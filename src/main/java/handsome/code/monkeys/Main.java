package handsome.code.monkeys;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import lombok.SneakyThrows;

@QuarkusMain
public class Main implements QuarkusApplication {
	@ConfigProperty(name = "limit", defaultValue = "5")
	long limit;

	private final String output = System.getProperty("user.dir") + "/../src/main/resources/output/";

	@ConfigProperty(name = "skip", defaultValue = "0")
	long skip;

	private String input(final String string) {
		return "/input/" + string;
	}

	@Override
	public int run(String... args) throws Exception {
		Stream.of("1_victoria_lake.txt", "2_himalayas.txt", "3_budapest.txt", "4_manhattan.txt", "5_oceania.txt")
				.skip(skip).limit(limit).map(this::input).map(getClass().getClassLoader()::getResource)
				.map(URL::getPath).map(Paths::get).forEach(this::write);
		return 0;
	}

	@SneakyThrows
	private void write(final Path path) {
		path.getName(path.getNameCount() - 1);
		Files.write(Paths.get(output + path.getName(path.getNameCount() - 1)),
				Files.lines(path).collect(Collectors.joining(System.lineSeparator())).getBytes());
	}
}
